package com.dxdev.testuran.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import static com.dxdev.testuran.R.layout.activity_folder;

public class FolderActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(activity_folder);
    }

}
