package com.dxdev.testuran.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.dxdev.testuran.R;
import com.dxdev.testuran.adapter.ListAdapter;
import com.dxdev.testuran.dialog.ActionsDialog;
import com.dxdev.testuran.model.ListModel;
import com.dxdev.testuran.sqlite.DBController;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private ListView mListView;
    private ListAdapter mAdapter;
    private List<ListModel> mList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mListView = (ListView) findViewById(R.id.lvMain);
        mList = new ArrayList<>();

        try {
            mList = DBController.onLoadFromBD(this);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (mList.size() == 0){
            onTestAddToList();
            DBController.onSaveToBD(this, mList);
        }

        mAdapter = new ListAdapter(this, mList);
        mListView.setAdapter(mAdapter);

        mListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                new ActionsDialog(MainActivity.this);
                return false;
            }
        });

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final ListModel model = mList.get(position);
                final String isFolder = model.getStrIsFolder();
                if (isFolder.length() > 1){
                    Toast.makeText(MainActivity.this, "File " + isFolder, Toast.LENGTH_SHORT).show();
                } else {
                    startActivity(new Intent(MainActivity.this, FolderActivity.class));
                }


            }
        });


    }

    private void onTestAddToList() {
        for (int count = 1; count < 10; count++) {

            String isFolder = "";
            boolean isBlue = false;
            boolean isOrange = false;
            if (count == 3 || count == 6){
                isFolder =  ".jpg";
                isBlue = true;
            }
            if (count == 2 || count == 8){
                isFolder =  ".pptx";
                isOrange = true;
            }

            if (count == 4){
                isFolder =  ".pptx";
                isBlue = true;
                isOrange = true;
            }

            mList.add(new ListModel(
                "name " + count,
                    isFolder,
                    new Date(),
                    isBlue,
                    isOrange
            ));
        }
    }

    @Override
    protected void onDestroy() {

        super.onDestroy();
    }
}
