package com.dxdev.testuran.sqlite;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.format.DateFormat;

import com.dxdev.testuran.model.ListModel;
import com.dxdev.testuran.resourcess.Constants;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DBController {

    public static void onSaveToBD(Activity activity, List<ListModel> list) {


        final DBHelper dbHelper = new DBHelper(activity);
        final ContentValues contentValues = new ContentValues();
        final SQLiteDatabase sqLiteDatabase = dbHelper.getWritableDatabase();

        for (ListModel model : list) {

            contentValues.put(Constants.BD_KEY_NAME, model.getStrFileName());
            contentValues.put(Constants.BD_KEY_IS_FOLDER, model.getStrIsFolder());
            contentValues.put(Constants.BD_KEY_DATE, (String) DateFormat.format(Constants.SIMPLE_DATE_FORMAT, model.getModDate()));
            contentValues.put(Constants.BD_KEY_IS_ORANGE, model.isOrange());
            contentValues.put(Constants.BD_KEY_IS_BLUE, model.isBlue());
            sqLiteDatabase.insert(Constants.BD_TABLE_NAME, null, contentValues);
        }

        dbHelper.close();
    }

    public static List<ListModel> onLoadFromBD(Activity activity) throws ParseException {


        final DBHelper dbHelper = new DBHelper(activity);
        final SQLiteDatabase sqLiteDatabase = dbHelper.getWritableDatabase();
        final Cursor cursor = sqLiteDatabase.query(Constants.BD_TABLE_NAME, null, null, null, null, null, null);

        final List<ListModel> list = new ArrayList<>();

        if (cursor.moveToFirst() && cursor.getCount() > 0) {

            final SimpleDateFormat format = new SimpleDateFormat(Constants.SIMPLE_DATE_FORMAT);

            do {
                Date date = null;
                boolean isBlue, isOrange;

                if (cursor.getString(cursor.getColumnIndex(Constants.BD_KEY_DATE)) != null) {
                    try {
                        date = format.parse(cursor.getString(cursor.getColumnIndex(Constants.BD_KEY_DATE)));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }

                if (cursor.getString(cursor.getColumnIndex(Constants.BD_KEY_IS_BLUE)).equals("1")) {
                    isBlue = true;
                } else {
                    isBlue = false;
                }
                if (cursor.getString(cursor.getColumnIndex(Constants.BD_KEY_IS_ORANGE)).equals("1")) {
                    isOrange = true;
                } else {
                    isOrange = false;
                }

                list.add(new ListModel(
                        cursor.getString(cursor.getColumnIndex(Constants.BD_KEY_NAME)),
                        cursor.getString(cursor.getColumnIndex(Constants.BD_KEY_IS_FOLDER)),
                        date,
                        isOrange,
                        isBlue
                ));

            } while (cursor.moveToNext());

            cursor.close();
        }

        dbHelper.close();


        return list;

    }

}
