package com.dxdev.testuran.sqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.dxdev.testuran.resourcess.Constants;


public class DBHelper extends SQLiteOpenHelper {

    public DBHelper(Context context) {
        super(context, Constants.BD_NAME, null, Constants.BD_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("create table " + Constants.BD_TABLE_NAME + " ("
                + Constants.BD_KEY_ID + " integer primary key autoincrement,"
                + Constants.BD_KEY_NAME       + " text,"
                + Constants.BD_KEY_IS_FOLDER  + " text,"
                + Constants.BD_KEY_DATE       + " date,"
                + Constants.BD_KEY_IS_ORANGE  + " text,"
                + Constants.BD_KEY_IS_BLUE    + " text"
                + ");");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

}