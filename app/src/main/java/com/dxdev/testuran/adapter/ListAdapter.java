package com.dxdev.testuran.adapter;

import android.app.Activity;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.BaseSwipeAdapter;
import com.dxdev.testuran.R;
import com.dxdev.testuran.dialog.ActionsDialog;
import com.dxdev.testuran.model.ListModel;
import com.dxdev.testuran.resourcess.Constants;

import java.util.List;


public class ListAdapter extends BaseSwipeAdapter {

    private MyHolder myHolder;
    private Activity mActivity;
    private List<ListModel> mList;
    private String[] isFolder;
    private boolean isSwipeOpen;

    public ListAdapter(Activity activity, List<ListModel> list) {
        this.mActivity = activity;
        this.mList = list;
        this.isFolder = Constants.FILE_TYPE;
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }

    @Override
    public View generateView(int position, ViewGroup parent) {
        View convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_item, parent, false);
        return convertView;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public void fillValues(final int position, final View view) {

        myHolder = (MyHolder) view.getTag();
        if (myHolder == null) {
            myHolder = new MyHolder(view);
            view.setTag(myHolder);
        }

        ListModel model = (ListModel) getItem(position);

        DateFormat.format(Constants.SIMPLE_DATE_FORMAT, model.getModDate());

        String fileType = "";

        if (model.getStrIsFolder().equalsIgnoreCase(isFolder[0])) {
            myHolder.ivIcon.setImageResource(R.drawable.image);
            fileType = isFolder[0];
        } else if (model.getStrIsFolder().equalsIgnoreCase(isFolder[1])) {
            myHolder.ivIcon.setImageResource(R.drawable.file);
            fileType = isFolder[1];
        } else {
            myHolder.ivIcon.setImageResource(R.drawable.folder);
        }

        myHolder.txtName.setText(model.getStrFileName() + fileType);
        myHolder.txtDate.setText(DateFormat.format(Constants.SIMPLE_DATE_FORMAT, model.getModDate()));

        if (model.isOrange() && model.isBlue()) {
            Log.d("LOgDD", "&&");
            myHolder.vFirstIndicator.setBackgroundColor(mActivity.getResources().getColor(R.color.colorOrange));
            myHolder.vSecondIndicator.setBackgroundColor(mActivity.getResources().getColor(R.color.colorBlue));
            
        }else if (model.isBlue()) {
            Log.d("LOgDD", "isBlue");
            myHolder.vFirstIndicator.setBackgroundColor(mActivity.getResources().getColor(R.color.colorBlue));
            myHolder.vSecondIndicator.setBackgroundColor(mActivity.getResources().getColor(R.color.colorBlue));
        } else if (model.isOrange()) {
            Log.d("LOgDD", "isOrange");
            myHolder.vFirstIndicator.setBackgroundColor(mActivity.getResources().getColor(R.color.colorOrange));
            myHolder.vSecondIndicator.setBackgroundColor(mActivity.getResources().getColor(R.color.colorOrange));
        }

        myHolder.swipeLayout.addSwipeListener(new SwipeLayout.SwipeListener() {
            @Override
            public void onStartOpen(SwipeLayout layout) {
            }

            @Override
            public void onOpen(SwipeLayout layout) {
                if (!isSwipeOpen) {
                    isSwipeOpen = true;
                    layout.close();
                    new ActionsDialog(mActivity);
                }
            }

            @Override
            public void onStartClose(SwipeLayout layout) {

            }

            @Override
            public void onClose(SwipeLayout layout) {
                layout.close();
                isSwipeOpen = false;
            }

            @Override
            public void onUpdate(SwipeLayout layout, int leftOffset, int topOffset) {
            }

            @Override
            public void onHandRelease(SwipeLayout layout, float xvel, float yvel) {

            }
        });

    }

    public class MyHolder {

        private TextView txtName, txtDate;
        private View vFirstIndicator, vSecondIndicator;
        private ImageView ivIcon;
        private SwipeLayout swipeLayout;


        public MyHolder(View view) {
            txtName = (TextView) view.findViewById(R.id.txtName);
            txtDate = (TextView) view.findViewById(R.id.txtDate);
            ivIcon = (ImageView) view.findViewById(R.id.ivIcon);
            vFirstIndicator = view.findViewById(R.id.vFirstIndicator);
            vSecondIndicator = view.findViewById(R.id.vSecondIndicator);
            swipeLayout = (SwipeLayout) view.findViewById(R.id.swipe);

        }
    }
}