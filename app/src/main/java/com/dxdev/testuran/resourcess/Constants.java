package com.dxdev.testuran.resourcess;

public class Constants {
    public static final String SIMPLE_DATE_FORMAT = ("MMMM dd, yyyy");
    public static final String[] FILE_TYPE = {".jpg", ".pptx"};

    public static final String BD_NAME = "myDB";
    public static final int BD_VERSION = 1;
    public static final String BD_TABLE_NAME = "table_list";

    public static final String BD_KEY_ID        = "_id";
    public static final String BD_KEY_NAME      = "fileName";
    public static final String BD_KEY_IS_FOLDER = "isFolder";
    public static final String BD_KEY_DATE      = "modDate";
    public static final String BD_KEY_IS_ORANGE = "isOrange";
    public static final String BD_KEY_IS_BLUE   = "isBlue";

}
