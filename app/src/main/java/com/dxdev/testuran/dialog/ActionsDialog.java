package com.dxdev.testuran.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.dxdev.testuran.R;

public class ActionsDialog implements View.OnClickListener{

    private View view;
    private LinearLayout llSetAsFavorite, llPermalink, llDelete;
    private Activity activity;

    private void findViewById(View view) {
        llSetAsFavorite = (LinearLayout) view.findViewById(R.id.llSetAsFavorite);
        llPermalink = (LinearLayout) view.findViewById(R.id.llPermalink);
        llDelete = (LinearLayout) view.findViewById(R.id.llDelete);
        llSetAsFavorite.setOnClickListener(this);
        llPermalink.setOnClickListener(this);
        llDelete.setOnClickListener(this);
    }

    public ActionsDialog(Activity activity) {
        this.activity = activity;
        show(activity);
    }

    private void show(final Activity activity) {

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        view = activity.getLayoutInflater().inflate(R.layout.dialog, null);
        builder.setView(view);

        findViewById(view);

        final Dialog alertDialog = builder.create();

        alertDialog.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.llSetAsFavorite:
                Toast.makeText(activity, "Set as favorite", Toast.LENGTH_SHORT).show();
                break;
            case R.id.llPermalink:
                Toast.makeText(activity, "Get permalink", Toast.LENGTH_SHORT).show();
                break;
            case R.id.llDelete:
                Toast.makeText(activity, "Delete", Toast.LENGTH_SHORT).show();
                break;
        }
    }
}