package com.dxdev.testuran.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;


public class ListModel implements Parcelable {

    public static final Creator<ListModel> CREATOR = new Creator<ListModel>() {
        @Override
        public ListModel createFromParcel(Parcel in) {
            return new ListModel(in);
        }

        @Override
        public ListModel[] newArray(int size) {
            return new ListModel[size];
        }
    };

    private String strFileName, strIsFolder;
    private Date modDate;
    private boolean isOrange, isBlue;


    public ListModel(String strFileName, String strIsFolder, Date modDate, boolean isOrange, boolean isBlue) {
        this.strFileName = strFileName;
        this.strIsFolder = strIsFolder;
        this.modDate = modDate;
        this.isOrange = isOrange;
        this.isBlue = isBlue;
    }



    protected ListModel(Parcel in) {
        strFileName = in.readString();
        strIsFolder = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public String getStrFileName() {
        return strFileName;
    }

    public String getStrIsFolder() {
        return strIsFolder;
    }

    public Date getModDate() {
        return modDate;
    }

    public boolean isOrange() {
        return isOrange;
    }

    public boolean isBlue() {
        return isBlue;
    }

    @Override
    public void writeToParcel(Parcel destParcel, int flags) {
        destParcel.writeString(strFileName);
        destParcel.writeString(strIsFolder);
    }

}
